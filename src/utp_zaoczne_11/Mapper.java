/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_11;


public interface Mapper<S> { // Uwaga: interfejs musi być sparametrtyzowany
    void map(S v);
}
