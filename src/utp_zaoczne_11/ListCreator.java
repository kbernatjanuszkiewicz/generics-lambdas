/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_11;


import java.util.ArrayList;
import java.util.List;

public abstract class ListCreator<T> { // Uwaga: klasa musi być sparametrtyzowana
    List<T> list;

    public ListCreator(List<T> list) {
        this.list = list;
    }

    //static<T, S> List<T> collectFrom(List<S> src)
    static <T> List<T> collectFrom(List<T> list) {
        return list;
    }

    static <T> List<T> when(List<T> src, Selector s) {
        List<T> listSelection = new ArrayList<T>();
        for(T n : src) {
            if(s.select(n)) {
                listSelection.add(n);
            }
        }
        return listSelection;
    }

    static <S> List<S> mapEvery(List<S> list, Mapper<S> m) {
        List<S> listMapped = new ArrayList<S>();
        for(S e : list) {
            m.map(e);
        }
        return listMapped;
    }


}
