/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_11;


public interface Selector<V> { // Uwaga: interfejs musi być sparametrtyzowany
     boolean select(V v);
}  
