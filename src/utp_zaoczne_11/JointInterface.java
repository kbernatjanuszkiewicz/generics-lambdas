package utp_zaoczne_11;

public interface JointInterface<V, S> extends Selector<V>, Mapper<S> {
    boolean select(V v);

    void map(S v);
}
